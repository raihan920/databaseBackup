<?php 
date_default_timezone_set("Asia/Dhaka");

$host = ''; //localhost
$user = ''; //your database user name should be here
$pass = ''; //your database password should be here
$name = ''; //your database name should be here


backup_tables($host, $user, $pass, $name);

/* backup the db OR just a table */

function backup_tables($host, $user, $pass, $name, $tables = '*') {

    $link = mysql_connect($host, $user, $pass);
    mysql_select_db($name, $link);

    //get all of the tables
    if ($tables == '*') {
        $tables = array();
        $result = mysql_query('SHOW TABLES');
        while ($row = mysql_fetch_row($result)) {
            $tables[] = $row[0];
        }
    } else {
        $tables = is_array($tables) ? $tables : explode(',', $tables);
    }
    
    $return = "";
    //cycle through
    foreach ($tables as $table) {
        
        $result = mysql_query('SELECT * FROM ' . $table);
        $num_fields = mysql_num_fields($result);
        $return .= 'DROP TABLE ' . $table . ';';
        $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
        $return .= "\n\n" . $row2[1] . ";\n\n";

        for ($i = 0; $i < $num_fields; $i++) {
            while ($row = mysql_fetch_row($result)) {
                $return .= 'INSERT INTO ' . $table . ' VALUES(';
                for ($j = 0; $j < $num_fields; $j++) {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                    if (isset($row[$j])) {
                        $return .= '"' . $row[$j] . '"';
                    } else {
                        $return .= '""';
                    }
                    if ($j < ($num_fields - 1)) {
                        $return .= ',';
                    }
                }
                $return .= ");\n";
            }
        }
        $return .= "\n\n\n";
    }
    
    //save file //    
    $sysDate = date("d-m-Y (D) h.i.s", time()); //taking date time input for file name
    $backupDirectory = "D:/DatabaseBackups/";   //taking directory input for saving file
    $handle = fopen($backupDirectory.$name.'-'.$sysDate.'-'.'.sql', 'x+'); //(md5(implode(',', $tables))).
    fwrite($handle, $return);
    fclose($handle);
    echo "Database Backed Up Successfully at ".$sysDate;
}
